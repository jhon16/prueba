
//Declaration of the dependences.
var mongoose = require("mongoose");
var userService = require('./../Services/personService');

//Creation of the functions for the methods
//Function of the method index, initial view of the people, (list people).
exports.index = function(req, res){
  userService.findUsers()
  .then(function(results){
    res.status(200).send(results);
  }, function(error){
    console.log('error: ' , error)
    res.status(400).send({
      error : error,
      message: error.message
    });
  })
  .catch(function(err){
    res.status(500).send('Error in the server.');
  })
};

//Function of the method create person
exports.create = function(req, res){
  userService.createUser(req.body)
  .then(function (result){
    res.status(200).send({data: result});
  },function (error){
    res.status(400).send(
    {
      error : error,
      message: error.message
    });
    console.log('The error to create person is: ', error);
  })
  .catch(function(err){
    res.status(500).send('Error in the server.');
  })
};

exports.show = function(req , res){
  userService.findEUser(req.params.id)
  .then(function (results){
    res.status(200).send(results);
  },function (error){
    console.log('error: ' , error)
    res.status(400).send({
      error : error,
      message: error.message
    });
  })
  .catch(function(err){
    res.status(500).send('Error in the server.');
    console.log('The error is: ', error);
  })
};

exports.update = function(req , res){
  userService.updateUser(req.params.id,req.body)
  .then(function (result){
    res.status(200).send({data: result});
  },function (error){
    res.status(400).send(
    {
      error : error,
      message: error.message
    })
    console.log('The error to update data person is: ', error);
  })
  .catch(function(err){
    res.status(500).send('Error in the server.')
  })
};

exports.delete = function(req , res){
  userService.deleteUser(req.params.id)
  .then(function (result){
    res.status(200).send({data: result});
  },function (error){
    res.status(400).send(
    {
      error : error,
      message: error.message
    })
    console.log('The error to delete data person is: ', error);
  })
  .catch(function(err){
    res.status(500).send('Error in the server.', err);
  })
};

exports.dateCalculation = function(req, res){
  userService.dateCalculation(req.params.id, req.query.field)
  .then(function(result){
    res.send(result);
  },function(error){
    res.send('Error when trying calculate the antiquity the one person in the company.');
    console.log('Error : ', error);
  })
  .catch(function(err){
    res.send('Error: ', err);
    res.send('Error in the server');
  })
};