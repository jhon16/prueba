var express = require('express');
var router = express.Router();
var cors = require('cors');
var login = require('./../Controller/authController');

router.post('/login', cors(), login.validate);

//exportation of the routes.
module.exports = router;