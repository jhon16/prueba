/* * 
 * @apiName RoutesPerson
 * @apiGroup Person
 */
//Declaration of the depedences.
var express = require('express');
var router = express.Router();
var cors = require('cors');
var person = require('./../Controller/personController');
var auth = require('./../Controller/authController');

//Declaration of the routes
/**
 * @api {get} /people |It request people information
 *
 * @apiSuccess Success-Response:
 *     HTTP/localhost3000/people OK
 *     {
 *       "name": "Jhon",
 *       "lastName": "Gonzalez",
 *       "gender": "Male",
 *       "dateBorn": "12/02/1998",
 *       "imagen": "Ulr Image",
 *       "dateStart": "15/12/2017",
 *       "salary": "800000"
 *     }
*/
router.get('/', cors(), person.index);

/*
 * @api {get} /people/:id |It request person information
 * @apiParam {Number} id person _id.
*/
router.get('/:id', cors(), person.show);

/*
 * @api {post} /people |It send person informacion
*/
router.post('/', cors(), person.create);

/*
 * @api {put} /people/:id |It send updated person information
 * @apiParam {Number} id id person _id.
*/
router.put('/:id', cors(), person.update);

/*
 * @api {put} /people/:id |It send updated person information
 * @apiParam {Number} id id person _id.
*/
router.put('/:id/password', cors(), auth.updated);

/*
 * @api {delete} /people/:id |It request delete person information
 * @apiParam {Number} id id person _id.  
*/
router.delete('/:id', cors(), person.delete);

/*
 * @api {get} /people/dateCaulculation:id |It Calculate age or antiquity of person
 * @apiParam {Number} id person _id.
*/
router.get('/dateCalculation/:id',cors(), person.dateCalculation);

//exportation of the routes.
module.exports = router;