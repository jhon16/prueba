var jwt = require('jsonwebtoken');

module.exports = function(req,res,next){
  //console.log('req.headers:   ',req.headers);
  //console.log('Ingreso middleware')
  var token = req.body.authorization || req.headers['authorization'];
  if (token) {
     token = token.split(' ')[1]
    jwt.verify(token, process.env.SECRET_KEY, function(err,decode){
      if (err) {
        res.status(401).send({error: 'Invalid token'});
      } else {
        next();
      }
    })
  } else {
    res.status(401).send({error: 'Invalid token'});
    res.redirect('/');
  }
}