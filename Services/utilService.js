//Declaration of the dependences.
var mongoose = require("mongoose");
//Declaration of the variable of the model.
var User = require('./../Model/User').model;
var Person = require('./../Model/Person').model;
//Declaration of the variable of the bcrypt.
var bcrypt = require('bcrypt-nodejs');

module.exports = {
  hashPassword: hashPassword,
  hashPasswordSync: hashPasswordSync,
  validatePassword: validatePassword,
  validateExistId: validateExistId
}

function hashPassword(password, callback){
  bcrypt.hash(password, bcrypt.genSaltSync(8), null, function(err, pswd){
    callback(err, pswd)
  })
}
function hashPasswordSync(password){
  return bcrypt.hashSync(password)
}

function validatePassword(string, hash, callback){
  bcrypt.compare(string, hash, function(err, pswd){
    callback(err, pswd)
  })
}

function validateExistId(id, callback) {
  Person.findOne({_id: id}, function(err, userFound){
    callback(err, userFound)
  })
}