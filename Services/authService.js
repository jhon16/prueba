//Declaration of the dependences.
var mongoose = require("mongoose");
//var NumberInt = require('mongoose-int32');

//Declaration of the variable of the model.
var User = require('./../Model/User').model;
var util = require('./utilService');

var jwt = require('jsonwebtoken');
var async = require('async');
var _ = require('lodash');

module.exports = {
  login: login,
  changePasssword: changePasssword,
  passwordHash: passwordHash,
  validateUserExist: validateUserExist
}

//Validate user.
function validateUserExist(data, callback){
  User.findOne({email: data.email}, function(err, userFound){
    callback(err, userFound, data)
  })
}
//validate password
function passwordValidation(user, data, callback){
  util.validatePassword(data.password, user.password, function(err, passwordValid){
    callback(err, passwordValid, user.password , data)
  })
}
//encrypt password
function passwordHash(passwordValid, password, data, callback){
  if(passwordValid){
    util.hashPassword(data.passwordNew, function(err, pswd){
      if(pswd){
        data.password=data.passwordNew, 
        data.passwordNew=pswd,
        callback(err, passwordValid, data)
      }
    })
  }else {
    callback("Password is wrong");
  }
}

//generate token
function generateToken(passwordValid, user, data, callback){
  if(passwordValid){
    var token = jwt.sign(data, process.env.SECRET_KEY,{
      expiresIn: 4000
    })
      callback(null, user , token  )
  }else callback("Password is wrong")
}

//login
function login (data){
  return new Promise(function(resolve, reject){
    async.waterfall([
      async.apply(validateUserExist, data),
      passwordValidation,
      generateToken
    ], function(err, res , token){
      if(err) reject(err);
      else resolve(data,data.token=token, data.passwordNew = res);
    })
  })
}

//update password user
function updatePassword(passwordValid, data, callback){
  if(passwordValid){
    User.update({email: data.email}, {password: data.passwordNew}, function(err, userUpdated){
      callback(err, userUpdated, data)
    })
  }else  callback("Password is wrong")
}



//change password user
function changePasssword (data){
  return new Promise(function(resolve, reject){
    async.waterfall([
      async.apply(validateUserExist, data),
      passwordValidation,
      passwordHash,
      updatePassword
    ], function(err, res, dat){
      if(err) reject(err);
      else resolve(dat);
    })
  })
}