//Declaration of the dependences.
var mongoose = require("mongoose");
var moment = require('moment');
var async = require('async');
var _ = require('lodash');
//var NumberInt = require('mongoose-int32');

//Declaration of the variable of the model.
var Person = require('./../Model/Person').model;
var User = require('./../Model/User').model;
var util = require('./utilService');

//Creation of the exportations of the functions or services. 
//console.log(Person)

module.exports = {
  createUser: createUser,
  updateUser: updateUser,
  findUsers: findUsers,
  findEUser: findEUser,
  deleteUser: deleteUser,
  dateCalculation: dateCalculation
}

//Creation of the service create person.
function createUser (data) {
  console.log('create data:' , data)
  return new Promise ( function(resolve, reject){
    async.waterfall([
      function passwordHash(callback){
        var info = _.clone(data)  
        if(info.name && info.lastName){
          var password = info.name + "" + info.lastName;
          util.hashPassword(password, function(err, pswd){
            if(pswd){
              info.passwordNew = password
              info.password = pswd
              if(err|| _.isEmpty(pswd)) callback('Error with the creation password')
              else callback(null,info)
            }
          })
        }else callback('firstName or lastName were not found')
      },
      function createUser(dt, callback){
        if(dt.email && dt.password){
          User.create(dt, function(err, userCreated){
            if (err || _.isEmpty(userCreated)) {
              callback('Error with the create user')
            }else {
              callback(null, userCreated._id)
            }        
          })
        }else{
          callback('the email was not found')
        }
      },
      function createPerson(uid, callback){
        data.uid = uid;
        Person.create(data, function(err, personCreated){
          if (err || _.isEmpty(personCreated)) callback('Error when create person')
          else callback(null, personCreated)
        })
      }
    ],function(err, res){
      if(err) reject(err);
      else resolve(res);
    })
  })
}

//Creation of the service update person.
function updateUser (id, data) {
  return new Promise ( function(resolve, reject){
    if(data.email)
    {
      reject('Cannot update the email.')
    }
    else{
      async.waterfall([
        function validateExistsId(callback){
          util.validateExistId(id, function(err, user){
            if(err || _.isEmpty(user)) callback('the user was not found')
            else callback(null, user)
          })
        },
        function updatedUser(user, callback){
          Person.update({_id: id}, data, function(err, userCreated){
            if (err) reject('Error when update person')
            else resolve(userCreated)
          })
        }
      ],
      function(err, res){
        if(err) reject(err);
        else resolve(res)
      })
    }
  })
}

//Creation of the service general query person.
function findUsers () {
  return new Promise ( function(resolve, reject){
    Person.find({}, function(err, usersFind){
      if (err) reject(err)
      else resolve(usersFind)
    })
  })
}

//Creation of the service specific query person.
function findEUser (id) {
  return new Promise ( function(resolve, reject){
    Person.findById({_id: id}, function(err, userFind){
      if (err || _.isEmpty(userFind)) reject("Error when find person")
      else resolve(userFind, "Find person corrently")
    })
  })
}

//Creation of the service delete person.
function deleteUser (id) {
  console.log(id)
  return new Promise ( function(resolve, reject){
    async.waterfall([
      function validateExistsId(callback){
        util.validateExistId(id, function(err, user){
          if(err || _.isEmpty(user)) callback('the user was not found')
          else callback(null, user)
        })
      },
      function deleteUser(user, callback){
        console.log('Entro en deleteUser, ', user)
        Person.remove({_id: id}, function(err, userFind){
          if (err) reject('Error when delete person')
          else resolve(userFind)
        })
      }
    ],
    function(err, res){
      if(err) reject(err);
      else resolve(res)
    })
  })
}

//Creation of the service specific query person.
function dateCalculation (id, field) {
  return new Promise ( function(resolve, reject){
    async.waterfall([
      function findStartDate(callback){  
        Person.findOne({_id: id}, function(err, userFound){
          if (err || _.isEmpty(userFound)) callback(err)
          else callback(null, userFound[field])
        })
      },
      function findBornDate(startDate,callback){  
        Person.findOne({_id: id}, function(err, userFound){
          if (err || _.isEmpty(userFound)) callback(err)
          else callback(null, startDate, userFound['dateBorn'])
        })
      },
      function calculate(startDate, bornDate, callback){
        var dateNow = moment();
        var dateStart = startDate;
        var dateBorn = bornDateM
        var difference2 = moment(dateNow).diff(moment(dateBorn));
        var difference = moment(dateNow).diff(moment(dateStart));
        var antiguity = moment.duration(difference).humanize();
        var age = moment.duration(difference2).humanize();
        callback(null, antiguity, age)
      }
    ], function(err, res, age){
      if(err) reject(err);
      else resolve(res, age);
    })
  })
}
