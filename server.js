//Declaration of the dependences.
var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');

//Declarate variable
var app = express(); 
var port = 3000;
var Person = require('./Routes/person');
var db = require('./DataAccess/Connection');
var bootstrap = require('./Bootstrap/bootstrap')
var login = require('./Routes/auth');
var middleware = require('./Routes/middleware');
process.env.SECRET_KEY = "mybadasskey";

//
app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(cors())
app.use(bodyParser.urlencoded({extended: true}));
app.options('/people',cors());
app.options('/people/:id',cors());

//it is connect the data base.
db.db;

bootstrap.bootstrap();

app.use('/', login);
app.use('/', middleware);
app.use('/people', Person);


//it is create the server and it is defined the port
app.listen(port);

//message informing that the server is working and the number of the port
console.log('The server is connected in the port: ' + port);
