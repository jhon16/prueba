var mongoose = require("mongoose");
const findOrCreate = require('mongoose-findorcreate')

//Creation of the Schema
var personSchema = new mongoose.Schema({
  name : {type: String, require: true},
  lastName : {type: String, require: true},
  email: {type: String, require: true, index: {unique: true}},
  gender : {type: String, require: false },
  dateBorn : {type: Date, require: true},
  image : {type: String, require: false},
  dateStart : {type: Date, require: true},
  salary : {type: Number, require: true},
  uid : {type: String, require: true, index: { unique : true }},
});

personSchema.plugin(findOrCreate);

//Declaration of the creation and/or called method of the schema.
exports.model = mongoose.model('Person', personSchema);