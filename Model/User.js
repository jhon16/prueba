var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
const findOrCreate = require('mongoose-findorcreate')

//Creation of the Schema
var UserSchema = new mongoose.Schema({

  email : {type: String, require: true, index: { unique: true}},
  password : {type: String, require: true},
  
});

UserSchema.plugin(findOrCreate);

exports.model = mongoose.model('User', UserSchema);

