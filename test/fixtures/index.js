module.exports = {
  "createPerson": {
		"name": "Jhon",
		"lastName": "Gonzalez",
		"gender": "M",
		"email": "jhon@gmail.com",
		"dateBorn": 914099408000,
		"dateStart": 1515789008000,
		"salary": 0
	},
  "createNoMail": {
		"name": "Jhon",
		"lastName": "Gonzalez",
		"gender": "M",
		"dateBorn": 914099408000,
		"dateStart": 1515789008000,
		"salary": 0
	},
	"createNoNames": {
		"email": "jhon.98@gmail.com",
		"gender": "M",
		"dateBorn": 914099408000,
		"dateStart": 1515789008000,
		"salary": 0
	},
  
  "id" : "5a4e50cc6ca58a1acf91ed43" ,

  "id2" : "12asdasdwe23e23",

  "id3" : "5a6267f19e9bfa2271c1c3b2",

  "updatePerson": {
        "name": "Jhon",
        "lastName": "Gonzalez",
        "gender": "M",
        "salary": 800000,
        "uid": "5a4e50a46ca58a1acf91ed42",
        "__v": 0,
        "dateStart": "1999-12-04T00:00:00.000Z",
        "dateBorn": "1990-02-11T00:00:00.000Z"
    },
  "updateWithMail": {
        "name": "Jhon Nicolas",
        "lastName": "Gonzalez Vallejo",
        "gender": "M",
        "email": "jhon.gonzal.26@gmail.com",
        "salary": 850000,
        "uid": "5a4e50a46ca58a1acf91ed42",
        "__v": 0,
        "dateStart": "1999-12-04T00:00:00.000Z",
        "dateBorn": "1990-02-11T00:00:00.000Z"
    }
}


