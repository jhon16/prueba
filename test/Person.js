var should = require('should');
var request = require('supertest');
var express = require('express');
var assert = require('chai').assert;
var fixtures = require('./fixtures/index');
var controller = require('./../Controller/personController');
var service = require('./../Services/personService');
const token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQFJva2szcmxhYnMuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkTWlOdm5hL0xDWHhieUFIdlc1RVZFdWROYVEvQlZIZkFXd25WanR6cXFCTVQzOVlVSFdRbC4iLCJpYXQiOjE1MTYzOTk1OTYsImV4cCI6MTUxNjQwMzU5Nn0.UnhHUoPy9OsNdYLdBID_KhzOfIA5k7dIDevbpt99J4o"
const url = "http://localhost:3000";
var headers = {"Content-Type": "application/json", "authorization": token}

var app = express();
//const server = require('./../server');
describe('Person', function(){

//Create
  app = url;


  it('Create person successfull', function(done){

    let data = JSON.stringify(fixtures.createPerson);
    request(app)  
    .post('/people')
    .send(data)
    .set('authorization', token)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(200)
    .end(function(err, res){
      if (err || res.body.error) {
        done(err);
      }
      else{
        assert.equal(res.body.data.email,fixtures.createPerson.email)
        done(null, 'upload successfull:'+ res.body.data); 
      }
    });
  });

  it('Error with the firstName/lastName are undefined, the password is not created', function(done){
    let data = JSON.stringify(fixtures.createNoNames);
    request(app)  
    .post('/people')
    .send(data)
    .set('authorization', token)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(400)
    .end(function(err, res){
      if (err || res.body.error) {
        assert.equal(res.body.error,'firstName or lastName were not found')
        done(null, 'upload failed:'+ res.body.error);
      }
      else{
        done(); 
      }
    });
  });

  it('Error when the user try to create user/person without email', function(done){
    let data = JSON.stringify(fixtures.createNoMail);
    request(app)  
    .post('/people')
    .send(data)
    .set('authorization', token)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(400)
    .end(function(err, res){
      if (err || res.body.error) {
        assert.equal(res.body.error,'the email was not found')
        done(null, 'upload failed:'+ res.body.error);
      }
      else{
        done(); 
      }
    });
  });

//Update 

  it('update person successfull', function(done){
    let data = JSON.stringify(fixtures.updatePerson);
    request(app)  
    .put('/people/'+fixtures.id)
    .send(data)
    .set('authorization', token)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(200)
    .end(function(err, res){
      if (err || res.body.error) {
        done();
      }
      else{
        assert.equal(res.body.data.ok, 1 )
        done(null, 'upload successfull:'+ res.body.data.ok); 
      }
    });
  });

  it('Error when the user try to update email', function(done){
    let data = JSON.stringify(fixtures.updateWithMail);
    request(app)  
    .put('/people/'+fixtures.id)
    .send(data)
    .set('authorization', token)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(400)
    .end(function(err, res){
      if (err || res.body.error) {
        assert.equal(res.body.error,'Cannot update the email.')
        done(null, 'upload failed:'+ res.body.error);
      }
      else{
        done(); 
      }
    });
  });

  it('Error when the user try update person without _id', function(done){
    let data = JSON.stringify(fixtures.updatePerson);
    request(app)  
    .put('/people/'+fixtures.id2)
    .send(data)
    .set('authorization', token)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(400)
    .end(function(err, res){
      if (err || res.body.error) {
        assert.equal(res.body.error,'the user was not found')
        done(null, 'upload failed:'+ res.body.error);
      }
      else{
        done(); 
      }
    });
  });

//Delete

  it('delete person successfull', function(done){
    request(app)  
    .delete('/people/'+fixtures.id3)
    .set('authorization', token)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(200)
    .end(function(err, res){
      if (err || res.body.error) {
        done();
      }
      else{
        console.log('res.body.data.ok: ', res.body.data.ok)
        assert.equal(res.body.data.ok,1)
        done(null, 'upload successfull:'+ res.body.data); 
      }
    });
  });

  it('Error when delete person', function(done){
    request(app)  
    .delete('/people/'+fixtures.id2)
    .set('authorization', token)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(400)
    .end(function(err, res){
      if (err || res.body.error) {
        assert.equal(res.body.error,'the user was not found')
        done(null, 'upload failed:'+ res.body.error);
      }
      else{
        done(); 
      }
    });
  });

//show people

  it('find person successfull', function(done){
    request(app)  
    .get('/people/'+fixtures.id)
    .set('authorization', token)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(200)
    .end(function(err, res){
      if (err || res.body.error) {
        done();
      }
      else{
        assert.equal(res.body._id,fixtures.id)
        done(null, 'upload successfull:'+ res.body.data); 
      }
    });
  });

  it('Error when find person', function(done){
    request(app)  
    .get('/people/'+fixtures.id2)
    .set('authorization', token)
    .set('Accept', 'application/json')
    .set('Content-Type', 'application/json')
    .expect(400)
    .end(function(err, res){
      if (err || res.body.error) {
        assert.equal(res.body.error,'Error when find person')
        done(null, 'upload failed:'+ res.body.error);
      }
      else{
        done(); 
      }
    });
  });
});