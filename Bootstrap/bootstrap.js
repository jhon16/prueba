var User = require('./../Model/User').model;
var async = require('async');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var util = require('./../Services/utilService');
var auth = require('./../Services/authService');

module.exports = {
    bootstrap: bootstrap
}

var data = {
  email: "admin@Rokk3rlabs.com",
  password: util.hashPasswordSync("Trudast55")
}

function bootstrap(){
  return new Promise (function(resolve, reject){
    async.waterfall([
      function userDefault(callback) {
        auth.validateUserExist(data, function(err, info){
          if(info){  
            callback(null, info);
          }else{
            User.create(data, function(err, userCreated) {
              if (err && _.isEmpty(userCreated)) {
                callback('Error with the create user')
              }else {
                callback(null, userCreated)
              }        
            })
          }
        })
      },
      function authotization(dta, callback) {
        var info = {
          email: dta.email,
          password: dta.password
        }
        var token = jwt.sign(info, process.env.SECRET_KEY,{
          expiresIn: 4000
        })
        console.log('token: ', token)
        callback(null,  token )
      }   
    ],
    function(err, tok) {
        if(err) reject(err);
        else resolve(tok);
    })
  })
}

